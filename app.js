let createError = require('http-errors');
let express = require('express');
let path = require('path');
let crypto = require('crypto');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let cors = require('cors')
let {errorHandlers, jwt} = require('./_helpers');

let usersRouter = require('./controller/UserController');

let app = express();
app.listen(process.env.LISTEN_PORT);

let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(cors(corsOptions));
app.use(jwt());

app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(errorHandlers);
module.exports = app;
