const {Op} = require('sequelize');
module.exports = {
    apps: [{
        name: 'Test Service',
        script: 'app.js',
        instances : 1,
        exec_mode : "fork",
        watch: true,
        error_file: "./logs/err.log",
        out_file: "./logs/out.log",
        merge_logs: true,
        log_date_format: "YYYY-MM-DD HH:mm Z",
        ignore_watch: [".idea", "node_modules", ".git", "uploads", "logs", "data", "export"],
        env_development: {
            LISTEN_PORT : 3200,
            NODE_ENV: 'development',
            SESSION: JSON.stringify({
                name: 'session',
                keys: ['key1', 'key2'],
                cookie: {
                    secure: true,
                    httpOnly: true,
                    domain: 'example.com',
                    path: 'foo/bar',
                    expires: new Date(Date.now() + 60 * 60 * 1000)
                }
            }),
            CONFIG: JSON.stringify({
                username: 'root',
                password: 'root',
                database: 'root',
                host: '192.168.99.100',
                port: '32769',
                dialect: 'postgres',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            }),
        },
        env_staging: {
            NODE_ENV: 'staging',
            STORAGE: './uploads',
            SESSION: JSON.stringify({
                name: 'session',
                keys: ['key1', 'key2'],
                cookie: {
                    secure: true,
                    httpOnly: true,
                    domain: 'example.com',
                    path: 'foo/bar',
                    expires: new Date(Date.now() + 60 * 60 * 1000)
                }
            }),
            CONFIG: JSON.stringify({
                username: 'staging',
                password: 'staging',
                database: 'staging',
                host: '192.168.99.100',
                port: '32772',
                dialect: 'postgres',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            }),
        },
        env_production: {
            LISTEN_PORT : 2300,
            NODE_ENV: 'production',
            STORAGE: './uploads',
            SESSION: JSON.stringify({
                name: 'session',
                keys: ['key1', 'key2'],
                cookie: {
                    secure: true,
                    httpOnly: true,
                    domain: 'example.com',
                    path: 'foo/bar',
                    expires: new Date(Date.now() + 60 * 60 * 1000)
                }
            }),
            CONFIG: JSON.stringify({
                username: 'test',
                password: 'bFz6IqLAm239e1cnBc8C100',
                database: 'test',
                host: 'postgresql_test',
                port: 6543,
                dialect: 'postgres',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            })
        }
    }]
};
